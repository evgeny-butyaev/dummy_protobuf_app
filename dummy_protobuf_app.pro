# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = dummy_protobuf_app

CONFIG += c++17 sailfishapp
QT += network positioning

protobuf_decl.name = protobuf header
protobuf_decl.input = PROTOS
protobuf_decl.output = ${QMAKE_FILE_BASE}.pb.h
protobuf_decl.commands = protoc --cpp_out="." --proto_path="$${IN_PWD}/proto" ${QMAKE_FILE_BASE}.proto
protobuf_decl.variable_out = GENERATED_FILES
QMAKE_EXTRA_COMPILERS += protobuf_decl

protobuf_impl.name = protobuf implementation
protobuf_impl.input = PROTOS
protobuf_impl.output = ${QMAKE_FILE_BASE}.pb.cc
protobuf_impl.depends = ${QMAKE_FILE_BASE}.pb.h
protobuf_impl.commands = $$escape_expand(\n)
protobuf_impl.variable_out = GENERATED_SOURCES
QMAKE_EXTRA_COMPILERS += protobuf_impl


SOURCES += \
	src/dummy_protobuf_app.cpp \
	src/navitag_connector/navitag_connector.cpp \
	src/geo_generator.cpp

HEADERS += \
	src/navitag_connector/navitag_connector.h \
	src/geo_generator.h

PROTOS += proto/tracker.proto

DISTFILES += qml/dummy_protobuf_app.qml \
	qml/cover/CoverPage.qml \
	qml/pages/FirstPage.qml \
	qml/pages/SecondPage.qml \
	rpm/dummy_protobuf_app.changes.in \
	rpm/dummy_protobuf_app.changes.run.in \
	rpm/dummy_protobuf_app.spec \
	rpm/dummy_protobuf_app.yaml \
	translations/*.ts \
	dummy_protobuf_app.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/dummy_protobuf_app-de.ts

LIBS += -lprotobuf
