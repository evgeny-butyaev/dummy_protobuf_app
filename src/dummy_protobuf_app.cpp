#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <QDebug>

#include <sailfishapp.h>

#include "navitag_connector/navitag_connector.h"
#include "geo_generator.h"


int main(int argc, char *argv[])
{
	// SailfishApp::main() will display "qml/dummy_protobuf_app.qml", if you need more
	// control over initialization, you can use:
	//
	//   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
	//   - SailfishApp::createView() to get a new QQuickView * instance
	//   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
	//   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
	//
	// To display the view, call "show()" (will show fullscreen on device).

	auto* app(SailfishApp::application(argc, argv));
	if (nullptr == app)
	{
		qDebug() << "Can not get app instance!";
		return 1;
	}

	/*
	 * Параметры подключения для коннектора задавать обязательно.
	 * Остальные параметрв (тайм-ауту, длина очереди) можно не менять, оставить по
	 * умолчанию. Однако можно и поменять, для чего есть соответствующие методы.
	 *
	 * Параметры можно менять и в процессе работы, однако они вступят в силу после
	 * перезапуска коннектора (вызова stop() и затем start())
	 */
	NavitagConnector nc(app);
	nc.setHost("193.232.47.4");
	nc.setPort(30159);
	nc.setDeviceID("6RLWW5S8C6V9");
	nc.setPlatform("Aurora OS");
	nc.setVersion("0.0.0.1");
	nc.start();

	/*
	 * Поскольку никаким способом получить под эмулятором Авроры геопозициюю мне не удалось
	 * (даже QGeoPositionInfoSource::createDefaultSource() возвращает nullptr), то
	 * я написал небольшой класс-генератор позиции, который кружит позицию по фигуре
	 * Лиссажу вокург стартовой точки.
	 */
	GeoGenerator gg(app,
					55.971223, 37.413412, // Шерематьего
//					59.800770, 30.264311, // Пулково
//					48.783067, 44.344390, // Аэропорт Волгограда
					120.0, // Высота (особенного значения не имеет, так, чтоб было)
					1.1, // Соотношение между периодами по X и Y. (1 - двигаться по кругу)
					2000 // Интервал между отправками данных в миллисекундах
					);

	QObject::connect(&gg, &GeoGenerator::newPosition, &nc, &NavitagConnector::enqueuePosition);
	gg.start();

	return app->exec();

	/*
	 * Если вызывать SailfishApp::main(argc, argv), то Qt не запустит цикл обработки
	 * сообщений и не будут работать асинхронные сигналы и слоты.
	 *
	 * При вызове app->exec() не создаётся и не загружается QML GUI, для его запуска
	 * необходимо вручнцю создавать его через SailfishApp::createView().
	 */
	//return SailfishApp::main(argc, argv);
}
