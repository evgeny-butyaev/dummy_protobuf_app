#include "geo_generator.h"

#include <cmath>

#include <QDateTime>

GeoGenerator::GeoGenerator(QObject* parent, double start_lat, double start_long, double start_alt, double freq_diff,
						   int interval)
	: QObject(parent)
	, start_lat_ {start_lat}
	, start_long_ {start_long}
	, start_alt_ {start_alt}
	, freq_diff_ {freq_diff}
	, interval_ {interval}
	, index_ {0}
	, timer_ {this}
{
	timer_.setSingleShot(false);
	connect(&timer_, &QTimer::timeout, this, &GeoGenerator::on_timer_timeout);
}

void GeoGenerator::start()
{
	timer_.start(interval_);
	on_timer_timeout();
}

void GeoGenerator::on_timer_timeout()
{
	QGeoPositionInfo gpi;

	QGeoCoordinate current {get_coordunate(index_)};
	QGeoCoordinate next {get_coordunate(index_ + 1)};

	gpi.setTimestamp(QDateTime::currentDateTime());
	gpi.setCoordinate(current);

	gpi.setAttribute(QGeoPositionInfo::Direction, current.azimuthTo(next));
	gpi.setAttribute(QGeoPositionInfo::GroundSpeed,
					 (current.distanceTo(next) * 1000.0f / static_cast<qreal>(interval_)) * 3.6f);

	emit newPosition(gpi);
	++index_;
}

QGeoCoordinate GeoGenerator::get_coordunate(quint64 index)
{
	QGeoCoordinate gc;

	static const double pi(std::acos(-1));

	double xa = static_cast<double>(index) * pi / 100.0;
	double ya = xa * freq_diff_;

	gc.setLatitude(start_lat_ + std::cos(xa) * 0.03);
	gc.setLongitude(start_long_ + std::sin(ya) * 0.06); // Мы на севере - надо чуть больше по долготе двигать!
	gc.setAltitude(start_alt_);

	return gc;
}
